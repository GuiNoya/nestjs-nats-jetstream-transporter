"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NatsTransportStrategy = void 0;
const tslib_1 = require("tslib");
const microservices_1 = require("@nestjs/microservices");
const common_1 = require("@nestjs/common");
const nats_1 = require("nats");
const nats_context_1 = require("./nats.context");
const nats_constants_1 = require("./nats.constants");
class NatsTransportStrategy extends microservices_1.Server {
    constructor(options = {}) {
        super();
        this.options = options;
        this.codec = options.codec || nats_1.JSONCodec();
        this.logger = new common_1.Logger(NatsTransportStrategy.name);
    }
    listen(callback) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.connection = yield this.createNatsConnection(this.options.connection);
            this.jetstreamClient = this.createJetStreamClient(this.connection);
            this.jetstreamManager = yield this.createJetStreamManager(this.connection);
            this.handleStatusUpdates(this.connection);
            yield this.subscribeToEventPatterns(this.jetstreamClient);
            this.subscribeToMessagePatterns(this.connection);
            this.logger.log(`Connected to ${this.connection.getServer()}`);
            callback();
        });
    }
    close() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this.connection) {
                yield this.connection.drain();
                this.connection = undefined;
                this.jetstreamClient = undefined;
                this.jetstreamManager = undefined;
            }
        });
    }
    createJetStreamClient(connection) {
        return connection.jetstream();
    }
    createJetStreamManager(connection) {
        return connection.jetstreamManager();
    }
    createNatsConnection(options = {}) {
        return nats_1.connect(options);
    }
    handleJetStreamMessage(message, handler) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const decoded = this.codec.decode(message.data);
            message.working();
            try {
                yield handler(decoded, new nats_context_1.NatsContext([message]))
                    .then((maybeObservable) => this.transformToObservable(maybeObservable))
                    .then((observable) => observable.toPromise());
                message.ack();
            }
            catch (error) {
                if (error === nats_constants_1.NACK) {
                    return message.nak();
                }
                if (error === nats_constants_1.TERM) {
                    return message.term();
                }
                throw error;
            }
        });
    }
    handleNatsMessage(message, handler) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const decoded = this.codec.decode(message.data);
            const maybeObservable = yield handler(decoded, new nats_context_1.NatsContext([message]));
            const response$ = this.transformToObservable(maybeObservable);
            this.send(response$, (response) => {
                const encoded = this.codec.encode(response);
                message.respond(encoded);
            });
        });
    }
    handleStatusUpdates(connection) {
        var e_1, _a;
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                for (var _b = tslib_1.__asyncValues(connection.status()), _c; _c = yield _b.next(), !_c.done;) {
                    const status = _c.value;
                    const data = typeof status.data === "object" ? JSON.stringify(status.data) : status.data;
                    const message = `(${status.type}): ${data}`;
                    switch (status.type) {
                        case "pingTimer":
                        case "reconnecting":
                        case "staleConnection":
                            this.logger.debug(message);
                            break;
                        case "disconnect":
                        case "error":
                            this.logger.error(message);
                            break;
                        case "reconnect":
                            this.logger.log(message);
                            break;
                        case "ldm":
                            this.logger.warn(message);
                            break;
                        case "update":
                            this.logger.verbose(message);
                            break;
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) yield _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
        });
    }
    subscribeToEventPatterns(client) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const eventHandlers = [...this.messageHandlers.entries()].filter(([, handler]) => handler.isEventHandler);
            for (const [pattern, handler] of eventHandlers) {
                const consumerOptions = nats_1.consumerOpts();
                if (this.options.consumer) {
                    this.options.consumer(consumerOptions);
                }
                consumerOptions.callback((error, message) => {
                    if (error) {
                        return this.logger.error(error.message, error.stack);
                    }
                    if (message) {
                        return this.handleJetStreamMessage(message, handler);
                    }
                });
                consumerOptions.deliverTo(nats_1.createInbox());
                consumerOptions.manualAck();
                try {
                    yield client.subscribe(pattern, consumerOptions);
                    this.logger.log(`Subscribed to ${pattern} events`);
                }
                catch (error) {
                    if (!(error instanceof nats_1.NatsError) || !error.isJetStreamError()) {
                        throw error;
                    }
                    if (error.message === "no stream matches subject") {
                        throw new Error(`Cannot find stream with the ${pattern} event pattern`);
                    }
                }
            }
        });
    }
    subscribeToMessagePatterns(connection) {
        const messageHandlers = [...this.messageHandlers.entries()].filter(([, handler]) => !handler.isEventHandler);
        for (const [pattern, handler] of messageHandlers) {
            connection.subscribe(pattern, {
                callback: (error, message) => {
                    if (error) {
                        return this.logger.error(error.message, error.stack);
                    }
                    return this.handleNatsMessage(message, handler);
                },
                queue: this.options.queue
            });
            this.logger.log(`Subscribed to ${pattern} messages`);
        }
    }
}
exports.NatsTransportStrategy = NatsTransportStrategy;
//# sourceMappingURL=nats.strategy.js.map