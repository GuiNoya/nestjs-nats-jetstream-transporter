import { CustomTransportStrategy, MessageHandler, Server } from "@nestjs/microservices";
import { Logger } from "@nestjs/common";
import { Codec, ConnectionOptions, JetStreamClient, JetStreamManager, JsMsg, Msg, NatsConnection } from "nats";
import { NatsTransportStrategyOptions } from "./interfaces/nats-transport-strategy-options.interface";
export declare class NatsTransportStrategy extends Server implements CustomTransportStrategy {
    protected readonly options: NatsTransportStrategyOptions;
    protected readonly codec: Codec<unknown>;
    protected readonly logger: Logger;
    protected connection?: NatsConnection;
    protected jetstreamClient?: JetStreamClient;
    protected jetstreamManager?: JetStreamManager;
    constructor(options?: NatsTransportStrategyOptions);
    listen(callback: () => void): Promise<void>;
    close(): Promise<void>;
    createJetStreamClient(connection: NatsConnection): JetStreamClient;
    createJetStreamManager(connection: NatsConnection): Promise<JetStreamManager>;
    createNatsConnection(options?: ConnectionOptions): Promise<NatsConnection>;
    handleJetStreamMessage(message: JsMsg, handler: MessageHandler): Promise<void>;
    handleNatsMessage(message: Msg, handler: MessageHandler): Promise<void>;
    handleStatusUpdates(connection: NatsConnection): Promise<void>;
    subscribeToEventPatterns(client: JetStreamClient): Promise<void>;
    subscribeToMessagePatterns(connection: NatsConnection): void;
}
